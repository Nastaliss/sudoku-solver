from functools import reduce
from math import floor


class Grid(object):

    def __init__(self, initial_values=[
        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],

        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],

        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],
        [0,0,0, 0,0,0, 0,0,0],
    ]):
        self.values = initial_values
        
    def _get_line(self, line_index):
        return self.values[line_index]

    def _get_column(self, column_index):
        return list(map(lambda line: line[column_index], self.values))

    def _get_block(self, block_index):
        start_line = floor(block_index / 3) * 3
        start_column = (block_index % 3) * 3
        return reduce(lambda previous_line, next_line: previous_line + next_line,
            map(lambda line: line[start_column: start_column + 3], self.values[start_line: start_line + 3])
        )

    def _number_already_on_line(self, number, line_index):
        return number in self._get_line(line_index)

    def _number_already_on_column(self, number, column_index):
        return number in self._get_column(column_index)
            
    def _number_already_on_block(self, number, block_index):
        return number in self._get_block(block_index)

    def _get_coordinates(self, cell_index):
        line = floor(cell_index / 9)
        column = cell_index % 9
        block = floor(column / 3) + floor(line/3) * 3
        return (line, column, block)
    
    def get_value(self, line, column):
        return self.values[line][column]
    
    def _set_value(self, line, column, value):
        self.values[line][column] = value

    def set_value_from_cell_index(self, cell_index, value):
        line, column, _ = self._get_coordinates(cell_index)
        self._set_value(line, column, value)

    def _fill_if_valid(self, cell_index):
        # print(cell_index)
        # EXIT STRATEGY
        if (cell_index >= 81):
            return True
        cell_line, cell_column, cell_block = self._get_coordinates(cell_index)

        # Cell is already filled
        if (self.get_value(cell_line, cell_column) != 0):
            return self._fill_if_valid(cell_index +1)
        
        # Fill
        for value in range(1, 10):
            # If this value doesn't fit, try the next value
            if  self._number_already_on_line(value, cell_line) or \
                self._number_already_on_column(value, cell_column) or \
                self._number_already_on_block(value, cell_block):
                continue
            self._set_value(cell_line, cell_column, value)
            # If this value worked, exit
            if self._fill_if_valid(cell_index + 1):
                return True

        # No value fitted, reset and backtrack
        self._set_value(cell_line, cell_column, 0)
        return False

    def fill_grid(self):
        self._fill_if_valid(0)
        return self.values

# grid = Grid(
#     [
#         [8,0,0, 0,0,0, 0,0,0],
#         [0,0,3, 6,0,0, 0,0,0],
#         [0,7,0, 0,9,0, 2,0,0],

#         [0,5,0, 0,0,7, 0,0,0],
#         [0,0,0, 0,4,5, 7,0,0],
#         [0,0,0, 1,0,0, 0,3,0],

#         [0,0,1, 0,0,0, 0,6,8],
#         [0,0,8, 5,0,0, 0,1,0],
#         [0,9,0, 0,0,0, 4,0,0],
#     ])


# print(grid.fill_grid())
