import os
import time
from copy import deepcopy
from time import time

import cv2
import numpy as np
import pytesseract as ts
from PIL import Image

from grid import Grid

# ─── CONSTANTS ──────────────────────────────────────────────────────────────────
TESSERACT_CONFIG = (
    '-l eng --oem 3 --psm 10 -c tessedit_char_whitelist=123456789')
FONT = cv2.FONT_HERSHEY_SIMPLEX
INPUT_IMAGE = 'sudoku.png'
start_time = time()
start_of_program_time = time()

print(f"Solving {INPUT_IMAGE}")

print(f"Opening image and applying basic transformations")

# ─── OPEN AND PRETREAT IMAGE ────────────────────────────────────────────────────
img = cv2.imread(os.path.join('assets', INPUT_IMAGE))
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)

# cv2.imwrite('pre.jpg', threshold)
print(f"Done in {time() - start_time} s")
start_time = time()
print(f"Finding contours and ordering cells")

# ─── FIND ALL 81 CELL BORDERS ───────────────────────────────────────────────────
contours, hierarchies = cv2.findContours(
    threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# Find the hierarchy corresponding to the sudoku cells' parent
hierarchy_parents = list(dict.fromkeys(hierarchies[0, :, 3]))
for hierarchy_parent in hierarchy_parents:
    if len(list(filter(lambda x: x[3] == hierarchy_parent, hierarchies[0]))) == 81:

        cell_parent = hierarchy_parent
        break
# Filter the contours corresponding to the sudoku cells
cell_contours = []
for contour_index, hierarchy in enumerate(hierarchies[0]):
    # Add cells which parent's is the main grid
    if hierarchy[3] == hierarchy_parent:
        cell_contours.append(contours[contour_index])

# Generate an image showing all cells' contours
# cv2.drawContours(img, cell_contours, -1, (0, 255, 0), 3)
# cv2.imwrite('contours81.jpg', img)

print(f"Ordering cells")


# ─── ORDER CELLS ────────────────────────────────────────────────────────────────
# Get grid borders (bounding rects)
min_x, max_x, min_y, max_y, max_step_x, max_step_y = (cv2.boundingRect(cell_contours[0])[0], 0, cv2.boundingRect(
    cell_contours[0])[1], 0, cv2.boundingRect(cell_contours[0])[2], cv2.boundingRect(cell_contours[0])[3])
# Find min and max coordinates (borders of the whole grid)
for contour in cell_contours:
    min_x = min(min_x, cv2.boundingRect(contour)[0])
    max_x = max(max_x, cv2.boundingRect(contour)[0])
    min_y = min(min_y, cv2.boundingRect(contour)[1])
    max_y = max(max_y, cv2.boundingRect(contour)[1])
    max_step_x = max(max_step_x, cv2.boundingRect(contour)[2])
    max_step_y = max(max_step_y, cv2.boundingRect(contour)[3])

width = max_x + max_step_x - min_x
height = max_y + max_step_y - min_y

# Sort cells by coordinates (left to right then top to bottom)
cell_contours = sorted(cell_contours, key=lambda contour: round(cv2.boundingRect(
    contour)[0] * 9 / width) + round(cv2.boundingRect(contour)[1] * 9 / height) * height)


print(f"Done in {time() - start_time} s")
start_time = time()

print(f"Reading exisiting numbers")

# ─── READ EXISTING NUMBERS ──────────────────────────────────────────────────────

grid = Grid()
for cell_index, cell_outline in enumerate(cell_contours):
    x, y, w, h = cv2.boundingRect(cell_outline)
    cell = gray[y + 2:y + h - 2, x + 2:x + w - 2]
    cell_image = Image.fromarray(cell)
    value = ts.image_to_string(cell_image, config=TESSERACT_CONFIG)
    if value in ['1', '2', '3', '4', '5', '6', '7', '8', '9']:
        grid.set_value_from_cell_index(cell_index, int(value))


print(f"Done in {time() - start_time} s")
start_time = time()

print(f"Solving grid")

# ─── SOLVE GRID ─────────────────────────────────────────────────────────────────
initial_grid = deepcopy(grid.values)
grid.fill_grid()

print(f"Done in {time() - start_time} s")
start_time = time()

print("Writing numbers back on grid")

# ─── WRITE SOLUTION ON INITIAL GRID ─────────────────────────────────────────────
result = deepcopy(img)
for line_index, line in enumerate(initial_grid):
    for column_index, cell in enumerate(line):
        if cell == 0:
            cell_index = column_index + line_index * 9
            x, y, w, h = cv2.boundingRect(cell_contours[cell_index])
            text = str(grid.get_value(line_index, column_index))
            thickness = 3

            (font_w, font_h), baseline = cv2.getTextSize(text, FONT, 1, thickness)
            offset_x = round(x + w/2 - font_w/2)
            offset_y = round(y + h/2 + baseline)
            result = cv2.putText(
                result, text, (offset_x, offset_y), FONT, 1, (200, 0, 0), thickness)

cv2.imwrite('Result.jpg', result)


print(f"Done in {time() - start_time} s")

print(f"Sudoku solved in {time() - start_of_program_time} s")
