# Sudoku Solver

*This project was created for the Image analysis and synthesis course at CentraleSupélec Rennes and the SERI major.*

## Goal

This program was created to solve sudoku grids passed as images.
It will compute all missing numbers and write them back on the original grid.

## How does it work ?
### Image processing
The input grid is read with opencv and a B&W filter as well as a threshold filter are applied before processing.

The contours feature of opencv is used to detect all the 81 individual cells of the grid, the hierarchy is taken into account to only grab these contours.

The content of the individual cells are then passed through tesseract (an OCR tool) to detect a number or a blank.

A 9x9 matrix is created and the blanks are replaced with zeros.

### Solver
A backtracking sudoku solver takes care of filling missing cells (for more info on backtracking follow [this link](https://en.wikipedia.org/wiki/Backtracking)).

### Image synthesis

The result is then written at the contours' positions. 
A new image is created (Result.jpg).

Project's original presentation can be seen [here](https://docs.google.com/presentation/d/1h6bgfByPR7qwbo-2-3D1TtKZppRA2HA6ldCZAWNnODo/edit?usp=sharing)
